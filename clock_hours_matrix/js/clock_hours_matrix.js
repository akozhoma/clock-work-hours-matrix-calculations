(function($) {
  $().ready(function() {
    var weeks = ['1', '2', '3', '4', '5']
    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
    var blocks = ['block-1', 'block-2', 'block-3']

    for (var k = 0, weeks_length = weeks.length; k < weeks_length; ++k) {
      $('table.week-' + weeks[k] + ' tr').each(function() {
        for (var i = 0, days_length = days.length; i < days_length; ++i) {
          if ($(this).hasClass(days[i])) {
            var inputTotal = $(this).find('input.' + days[i] + '.total')
            var inputStartBlock1 = $(this).find('td.' + days[i] + '.block-1 input.start')
            var inputFinishBlock1 = $(this).find('td.' + days[i] + '.block-1 input.finish')
            var inputHiddenBlock1 = $(this).find('td.' + days[i] + '.block-1 input.sum-hidden')
            var inputStartBlock2 = $(this).find('td.' + days[i] + '.block-2 input.start')
            var inputFinishBlock2 = $(this).find('td.' + days[i] + '.block-2 input.finish')
            var inputHiddenBlock2 = $(this).find('td.' + days[i] + '.block-2 input.sum-hidden')
            var inputStartBlock3 = $(this).find('td.' + days[i] + '.block-3 input.start')
            var inputFinishBlock3 = $(this).find('td.' + days[i] + '.block-3 input.finish')
            var inputHiddenBlock3 = $(this).find('td.' + days[i] + '.block-3 input.sum-hidden')
            var inputAllBlocks = $(this).find('input')
            $(inputAllBlocks).each(function() {
              $(this).change(function() {
                if($(this).val() < 0 || $(this).val() > 24) {
                  $(this).css('border', '1px solid red')
                  alert(Drupal.t('The time value is incorrect. Enter hours from 00.00 to 24.00 and use a period (.) as a separator stabbing between the hours and minutes'))
                }
                else
                  $(this).css('border', '1px solid #ccc')
              })
            })
            $(inputAllBlocks).each(function() {
              $(this).change(function() {
                if ((($(inputStartBlock1).val().length > 0) && ($(inputFinishBlock1).val().length > 0))
                  || (($(inputStartBlock2).val().length > 0) && ($(inputFinishBlock2).val().length > 0))
                  || (($(inputStartBlock3).val().length > 0) && ($(inputFinishBlock3).val().length > 0))
                ) {
                  var valBlock1Start = parseFloat($(inputStartBlock1).val())
                  var valBlock1Finish = parseFloat($(inputFinishBlock1).val())
                  var valBlock2Start = parseFloat($(inputStartBlock2).val())
                  var valBlock2Finish = parseFloat($(inputFinishBlock2).val())
                  var valBlock3Start = parseFloat($(inputStartBlock3).val())
                  var valBlock3Finish = parseFloat($(inputFinishBlock3).val())
                  var total1 = 0
                  var total2 = 0
                  var total3 = 0
                  alertFinishLowerStartTime(inputStartBlock1, inputFinishBlock1, inputHiddenBlock1, valBlock1Start, valBlock1Finish)
                  alertFinishLowerStartTime(inputStartBlock2, inputFinishBlock2, inputHiddenBlock2, valBlock2Start, valBlock2Finish)
                  alertFinishLowerStartTime(inputStartBlock3, inputFinishBlock3, inputHiddenBlock3, valBlock3Start, valBlock3Finish)
//                  if(valBlock1Start > valBlock1Finish || valBlock2Start > valBlock2Finish || valBlock3Start > valBlock3Finish) {
//                    $(inputStartBlock1).css('border', '1px solid yellow')
//                    $(inputFinishBlock1).css('border', '1px solid yellow')
//                    alert(Drupal.t('End time cannot be lower than the start time.'))
//                  } else
//                    $(this).css('border', '1px solid #ccc')

                  if(!isNaN(valBlock1Finish) && !isNaN(valBlock1Start))
                    total1 = Number(valBlock1Finish - valBlock1Start, 2).toFixed(2)

                  if(!isNaN(valBlock2Finish) && !isNaN(valBlock2Start))
                    total2 = Number(valBlock2Finish - valBlock2Start, 2).toFixed(2)

                  if(!isNaN(valBlock3Finish) && !isNaN(valBlock3Start))
                    total3 = Number(valBlock3Finish - valBlock3Start, 2).toFixed(2)

                  if (valBlock1Finish >= 0 && valBlock1Finish <= 24 && valBlock1Start >= 0 && valBlock1Start <= 24 && valBlock1Finish > valBlock1Start)
                    $(inputHiddenBlock1).val(total1)
                  else
                    $(inputHiddenBlock1).val('')

                  if (valBlock2Finish >= 0 && valBlock2Finish <= 24 && valBlock2Start >= 0 && valBlock2Start <= 24 && valBlock2Finish > valBlock2Start)
                    $(inputHiddenBlock2).val(total2)
                  else
                    $(inputHiddenBlock2).val('')

                  if (valBlock3Finish >= 0 && valBlock3Finish <= 24 && valBlock3Start >= 0 && valBlock3Start <= 24 && valBlock3Finish > valBlock3Start)
                    $(inputHiddenBlock3).val(total3)
                  else
                    $(inputHiddenBlock3).val('')

                  var totalResult = 0
                  var totalValBlock1 = parseFloat($(inputHiddenBlock1).val())
                  var totalValBlock2 = parseFloat($(inputHiddenBlock2).val())
                  var totalValBlock3 = parseFloat($(inputHiddenBlock3).val())
                  if (!isNaN(totalValBlock1))
                    totalResult += totalValBlock1

                  if (!isNaN(totalValBlock2))
                    totalResult += totalValBlock2

                  if (!isNaN(totalValBlock3))
                    totalResult += totalValBlock3

                  if (totalResult == 0)
                    $(inputTotal).val('')
                  else
                    $(inputTotal).val(totalResult)
                }
              })
            })
          }
        }
      })
    }
    function alertFinishLowerStartTime(startSelector, finishSelector, sumSelector, startValue, finishValue) {
      if(startValue > finishValue) {
        $(startSelector).css('border', '1px solid yellow')
        $(finishSelector).css('border', '1px solid yellow')
        alert(Drupal.t('End time cannot be lower than the start time.'))
      } else {
        $(startSelector).css('border', '1px solid #ccc')
        $(finishSelector).css('border', '1px solid #ccc')
      }
    }
  })
})(jQuery)