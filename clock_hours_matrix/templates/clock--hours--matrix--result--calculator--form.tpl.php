
<?php $total_week_number = variable_get('total_week_number'); ?>
<table id="calculator-result" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td colspan="9">
        <table cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td class="Calclicht" width="150px"></td>
              <td class="Calclicht" align="center" width="150px"><?php print t('total time in hours'); ?></td>
              <td class="Calclicht" align="center" width="150px"><?php print t('total pay hours'); ?></td>
              <td class="Calclicht" align="center" width="150px"><?php print t('surcharge rate'); ?></td>
            </tr>
            <?php for ($i = 1; $i <= $total_week_number; $i++): ?>
              <?php
                $id_week_total_time_hours = 'result-total-time-hours-week-' . $i;
                $id_week_total_pay_hours = 'result-total-pay-hours-week-' . $i;
                $id_week_total_surcharge_rate = 'result-total-surcharge-rate-week-' . $i;
              ?>
              <tr>
                <td class="Calclicht" width="150px"><?php print t('Week') . ' '. $i; ?></td>
                <td class="Calclicht" align="center" width="150px"><?php print render($form[$id_week_total_time_hours]); ?></td>
                <td class="Calclicht" align="center" width="150px"><?php print render($form[$id_week_total_pay_hours]); ?></td>
                <td class="Calclicht" align="center" width="150px"><?php print render($form[$id_week_total_surcharge_rate]); ?></td>
              </tr>
            <?php endfor; ?>
            <tr>
              <td colspan="3"><?php print t('average fee rate'); ?></td>
              <td class="Calclicht" align="center"><?php print render($form['result-average-fee-rate']); ?></td>
            </tr>
            <tr>
              <td colspan="3"><?php print t('correction (only two shifts)'); ?></td>
              <td class="Calclicht" align="center"><?php print render($form['result-correction']); ?></td>
            </tr>
            <tr>
              <td colspan="3"><?php print t('total average fee rate'); ?></td>
              <td class="Calclicht" align="center"><?php print render($form['result-total-average-fee-rate']); ?></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<div>
  <?php
    print render($form['form_build_id']);
    print render($form['form_token']);
    print render($form['form_id']);
    print render($form['#validate']);
    print render($form['actions']);
  ?>
</div>