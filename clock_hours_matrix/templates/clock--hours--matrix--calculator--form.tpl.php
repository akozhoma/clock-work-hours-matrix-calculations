
<?php $week = $form['current_step']['#value']; ?>
<table id="calculator" border="0" cellpadding="0" cellspacing="0">						
  <tbody>
    <tr>
      <td rowspan="2" align="center" style="text-align: center;" width="30px"><?php print t('wk'); ?></td>
      <td rowspan="2" align="center" style="text-align: center;" width="80px"><?php print t('day'); ?></td>
      <?php foreach (clock_hours_matrix_work_blocks() as $block): ?>
        <td align="center" colspan="2" width="130px" style="text-align: center;"><?php print $block; ?></td>
      <?php endforeach; ?>
      <td rowspan="2" align="center" width="60px" style="text-align: center;"><?php print t('total time in hours'); ?></td>
    </tr>
    <tr>
      <?php foreach (clock_hours_matrix_work_blocks() as $block): ?>
        <td align="center" width="65px" style="text-align: center;"><?php print t('start time'); ?></td>
        <td align="center" width="65px" style="text-align: center;"><?php print t('end time'); ?></td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <td width="30px" align="center" valign="middle" style="text-align: center;"><?php print $week; ?></td>
      <td colspan="8" width="530px" style="padding: 0; margin: 0; text-align: center;">
        <table id="calc-widget" cellpadding="0" cellspacing="0" class="<?php print t('week') . '-' . $week; ?>">
          <tbody>
            <?php foreach (clock_hours_matrix_week_days() as $day): ?>
              <tr class="<?php print clock_hours_matrix_str_transform($day); ?>">
                <td width="74px" style="border-left: none; border-top: none;"><?php print $day ?></td>
                <?php foreach (clock_hours_matrix_work_blocks() as $block): ?>
                  <?php
                    $form_field_name_start = clock_hours_matrix_str_transform("week-" .$week . "-" . $block . "-" . $day . "-start");
                    $form_field_name_finish = clock_hours_matrix_str_transform("week-" .$week . "-" . $block . "-" . $day . "-finish");
                    $form_field_name_hidden_sum = clock_hours_matrix_str_transform("week-" . $week . "-" . $block . "-" . $day . "-hidden-sum");
                  ?>
                  <td class="<?php print clock_hours_matrix_str_transform($day) . ' ' . clock_hours_matrix_str_transform($block); ?>" colspan="2" width="135px" align="middle" style="text-align: center; border-top: none;">
                    <div style="float: left;"><?php print render($form[$form_field_name_start]); ?></div>
                    <div style="float: left; width: 25px; margin-top: 5px;"> - </div>
                    <div style="float: right;"><?php print render($form[$form_field_name_finish]); ?></div>
                    <?php print render($form[$form_field_name_hidden_sum]); ?>
                  </td>
                <?php endforeach; ?>
                <?php $form_field_name_total = clock_hours_matrix_str_transform("week-" .$week . "-" . $day . "-total"); ?>
                <td width="55px" align="center" style="border-right: none; border-top: none;">
                  <?php print render($form[$form_field_name_total]); ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<div>
  <?php
    print render($form['form_build_id']);
    print render($form['form_token']);
    print render($form['form_id']);
    print render($form['#validate']);
    print render($form['actions']);
  ?>
</div>